<?php

namespace App\DataFixtures;

use App\Entity\Advert;
use App\Entity\Category;
use App\Entity\Material;
use App\Entity\User;
use App\Entity\WheelSize;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{

    const ADVERT_NUMBER = 100;

    public function __construct(private UserPasswordHasherInterface $passwordHasher)
    {
    }


    public function load(ObjectManager $manager): void
    {

        $user1 = new User();
        $user1->setEmail("first@localhost");
        $password = $this->passwordHasher->hashPassword(
            $user1,
            "user1"
        );
        $user1->setPassword($password);
        $user1->setRoles(['ROLE_USER']);

        $user2 = new User();
        $user2->setRoles(['ROLE_ADMIN']);
        $user2->setEmail("admin@localhost");
        $password = $this->passwordHasher->hashPassword(
            $user2,
            "user2"
        );
        $user2->setPassword($password);
        $manager->persist($user1);
        $manager->persist($user2);

        $users = [$user1, $user2];

        $categories = ['XC', 'Enduro', 'DH', 'Gravel', 'Route', 'Cyclo-X'];
        $categoriesEntities = [];
        foreach ($categories as $categoryName) {
            $category = new Category();
            $category->setName($categoryName);
            $manager->persist($category);
            $categoriesEntities[] = $category;
        }

        $sizes = [12, 16, 20, 24, 26, 27.5, 28, 29];
        $sizesEntities = [];
        foreach ($sizes as $size) {
            $wheelSize = new WheelSize();
            $wheelSize->setNumberOfInches($size);
            $manager->persist($wheelSize);
            $sizesEntities[] = $wheelSize;
        }

        $materials = ['Carbone', 'Titane', 'Acier', 'Aluminium', 'Fourche carbone'];
        $materialsEntities = [];
        foreach ($materials as $materialName) {
            $material = new Material();
            $material->setName($materialName);
            $manager->persist($material);
            $materialsEntities[] = $material;
        }

        $brands = ['Scott', 'Cube', 'Specialized', 'Orbea'];

        for($i=0; $i < self::ADVERT_NUMBER; $i++) {

            $advert = new Advert();
            $advert->setCategory($categoriesEntities[$i % count($categoriesEntities)]);
            $advert->setWheelSize($sizesEntities[$i % count($sizesEntities)]);
            $advert->setMaterial($materialsEntities[$i % count($materialsEntities)]);
            $advert->setUser($users[$i % count($users)]);
            $advert->setTitle('Annonce n°'.$i);
            $advert->setDescription('Description de l\'annonce n°'.$i);
            $advert->setBrand($brands[$i % count($brands)]);
            $advert->setYear(2023);
            $manager->persist($advert);

        }
        $manager->flush();

    }
}
