<?php

namespace App\Repository;

use App\Entity\WheelSize;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<WheelSize>
 *
 * @method WheelSize|null find($id, $lockMode = null, $lockVersion = null)
 * @method WheelSize|null findOneBy(array $criteria, array $orderBy = null)
 * @method WheelSize[]    findAll()
 * @method WheelSize[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WheelSizeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WheelSize::class);
    }

//    /**
//     * @return WheelSize[] Returns an array of WheelSize objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('w.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?WheelSize
//    {
//        return $this->createQueryBuilder('w')
//            ->andWhere('w.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
