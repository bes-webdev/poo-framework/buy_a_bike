<?php

namespace App\Twig;

use App\Entity\Advert;
use App\Entity\Category;
use App\Form\SearchType;
use App\Repository\CategoryRepository;
use Symfony\Component\Asset\Packages;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class MyTwigExtension extends AbstractExtension
{
    private Packages $assetsManager;
    private CategoryRepository $categoryRepository;
    private FormFactoryInterface $formFactory;
    private RouterInterface $router;
    private string $assetFileFolderName;

    public function __construct(Packages $assetsManager, string $assetFileFolderName, CategoryRepository $categoryRepository, FormFactoryInterface $formFactory, RouterInterface $router)
    {
        $this->assetsManager = $assetsManager;
        $this->assetFileFolderName = $assetFileFolderName;
        $this->categoryRepository = $categoryRepository;
        $this->router = $router;
        $this->formFactory = $formFactory;
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('photoOrDefault', [$this, 'photoOrDefault']),
            new TwigFunction('getCategories', [$this, 'getCategories']),
//            new TwigFunction('getSearchForm', [$this, 'getSearchForm']),
        ];
    }


    public function photoOrDefault(Advert $advert) {

        if ($advert->getPhotos() != null && count($advert->getPhotos())) {
            return $this->assetsManager->getUrl($this->assetFileFolderName.'/'.$advert->getPhotos()[0]->getPath());
        } else {
            return $this->assetsManager->getUrl('assets/images/default.jpg');
        }
    }

    public function getCategories(): array
    {
        return $this->categoryRepository->findAll();
    }

//    public function getSearchForm() {
//        $form = $this->formFactory->create(SearchType::class, null);
//        return $form->createView();
//    }


}
