<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Entity\BlogPost;
use App\Entity\Comment;
use App\Form\CommentType;
use App\Form\SearchType;
use App\Repository\AdvertRepository;
use App\Repository\BlogPostRepository;
use App\Repository\CategoryRepository;
use App\Repository\UserRepository;
use App\Search\SearchData;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function index(AdvertRepository $advertRepository): Response
    {

        $adverts = $advertRepository->findBy([], ['id' => 'DESC'], 18);

        return $this->render('pages/home.html.twig', ['adverts' => $adverts]);
    }

        #[Route('/category/{categoryName}', name: 'category')]
    public function category(string $categoryName, CategoryRepository $categoryRepository, AdvertRepository $advertRepository): Response {

        $category = $categoryRepository->findOneBy(['name' => $categoryName]);

        $adverts = $advertRepository->findBy(['category' => $category]);

        return $this->render('pages/category.html.twig', ['category' => $category, 'adverts' => $adverts]);
    }

    #[Route('/a/{slug}', name: 'advert')]
    public function viewAdvert(string $slug, AdvertRepository $advertRepository) {

        $advert = $advertRepository->findOneBy(['slug' => $slug]);

        $form = $this->createForm(CommentType::class, null, ['action' => $this->generateUrl('create_advert_comment', ['id' => $advert->getId()])]);

        return $this->render('pages/advert.html.twig', ['advert' => $advert, 'commentForm' => $form->createView()]);
    }

    #[Route('/a/{id<\d+>}/comments', name: 'get_advert_comments')]
    public function getAdvertComments(Advert $advert, SerializerInterface $serializer) {

        return new JsonResponse($serializer->serialize($advert->getComments(), 'json', [
            'circular_reference_handler' => function ($object) {
                return $object->getId();
            }
        ]));
    }

    #[Route('/a/{id<\d+>}/comment-form', name: 'get_advert_comment_form')]
    public function getAdvertCommentsForm(Advert $advert, SerializerInterface $serializer) {

        $form = $this->createForm(CommentType::class, null, ['action' => $this->generateUrl('create_advert_comment', ['id' => $advert->getId()])]);

        return $this->render('pages/includes/comment_form.html.twig', ['commentForm' => $form->createView()]);
    }

    #[Route('/comment/{id<\d+>}', name: 'create_advert_comment')]
    public function createAdvertComment(
        $id,
        Request $request,
        EntityManagerInterface $em,
        AdvertRepository $advertRepository,
        UserRepository $userRepository,
        SerializerInterface $serializer)
    {
        $advert = $advertRepository->find($id);
        if ($advert === null) {
            throw new NotFoundHttpException();
        }

        $user = $userRepository->findAll();

        $comment = new Comment();
        $comment->setAdvert($advert);
        $comment->setAuthor($user[0]);

        $form = $this->createForm(CommentType::class, $comment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($comment);
            $em->flush();
            return new JsonResponse($serializer->serialize($comment, 'json', [
                'circular_reference_handler' => function ($object) {
                    return $object->getId();
                }
            ]));
        }
    }

    #[Route(path: "/search", name: 'app_search')]
    public function search(Request $request, AdvertRepository $advertRepository) {
        $search = new SearchData();
        $form = $this->createForm(SearchType::class, $search, ['action' => $this->generateUrl('app_home')]);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $results = $advertRepository->search($search);
        } else {
            $results = [];
        }

        return $this->render('pages/search.html.twig', ['form' => $form->createView(), 'results' => $results    ]);
    }


}
