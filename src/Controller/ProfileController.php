<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Form\AdvertType;
use App\Form\ProfileType;
use App\Repository\AdvertRepository;
use App\Service\PhotoFormHandler;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProfileController extends AbstractController
{

    #[Route('/profile/my-adverts', name: 'profile_my_adverts')]
    public function myAdverts(AdvertRepository $advertRepository, EntityManagerInterface $em) {
        $em->getFilters()->disable('softdeleteable');
        $adverts = $advertRepository->findBy(['user' => $this->getUser()]);
        return $this->render('pages/admin/my-adverts.html.twig', ['adverts' => $adverts]);
    }

    #[Route('/profile/my-profile', name: 'profile_my_profile')]
    public function myProfile(EntityManagerInterface $em, Request $request) {
        $user = $this->getUser();
        $form = $this->createForm(ProfileType::class, $user, []);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($user);
            $em->flush();
            return $this->redirectToRoute('profile_my_profile');
        }
        return $this->render('pages/admin/my-profile.html.twig', ['profile_form' => $form->createView()]);

    }

    #[Route('/profile/advert/new', name: 'advert_create')]
    public function createAdvert(EntityManagerInterface $em, Request $request, PhotoFormHandler $photoFormHandler) {
        $advert = new Advert();
        $form = $this->createForm(AdvertType::class, $advert);
        dump($form);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uploadedPhotosForm = $form->get('photos');
            foreach ($uploadedPhotosForm as $photoForm) {
                $photoFormHandler->uploadFilesFromForm($photoForm);

            }
            foreach ($advert->getPhotos() as $photo) {
                $photo->setAdvert($advert);
            }
            $em->persist($advert);
            $em->flush();
            return $this->redirectToRoute('advert', ["slug" => $advert->getSlug()]);
        }

        return $this->render('pages/admin/advert.html.twig', ['myAdvertForm' => $form->createView()]);
    }


    #[Route('/profile/advert/{id}/delete', name: 'advert_remove')]
    public function removeAdvert(Advert $advert, EntityManagerInterface $em) {
        $em->remove($advert);
        $em->flush();
        return $this->redirectToRoute('profile_my_adverts');
    }

    #[Route('/profile/advert/{id}/come-back', name: 'advert_comeback')]
    public function reviensAdvert(int $id, EntityManagerInterface $em, AdvertRepository $repo) {
        $em->getFilters()->disable('softdeleteable');
        $advert = $repo->find($id);
        $advert->setDeletedAt(null);
        $em->persist($advert);
        $em->flush();
        return $this->redirectToRoute('profile_my_adverts');
    }
    #[Route('/profile/advert/{id}', name: 'advert_edit')]
    public function editAdvert(int $id, AdvertRepository $advertRepository, EntityManagerInterface $em, Request $request, PhotoFormHandler $photoFormHandler) {

        $advert = $advertRepository->find($id);

        if ($advert === null) {
            return $this->redirectToRoute('advert_create');
        }

        if (!$this->isGranted('ROLE_ADMIN', $this->getUser()) && $advert->getUser() !== $this->getUser()) {
            throw new AccessDeniedException("Not your advert");
        }

        $form = $this->createForm(AdvertType::class, $advert);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $uploadedPhotosForm = $form->get('photos');
            foreach ($uploadedPhotosForm as $photoForm) {
                $photoFormHandler->uploadFilesFromForm($photoForm);

            }
            foreach ($advert->getPhotos() as $photo) {
                $photo->setAdvert($advert);
            }

            $em->persist($advert);
            $em->flush();
            return $this->redirectToRoute('advert', ["slug" => $advert->getSlug()]);
        }

        return $this->render('pages/admin/advert.html.twig', ['myAdvertForm' => $form->createView()]);
    }

}
