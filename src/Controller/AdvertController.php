<?php

namespace App\Controller;

use App\Entity\Advert;
use App\Entity\Photo;
use App\Form\AdvertType;
use App\Form\PhotoType;
use App\Repository\AdvertRepository;
use App\Repository\PhotoRepository;
use App\Service\PhotoFormHandler;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Integer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AdvertController extends AbstractController
{




    #[Route('/profile/photo', name: 'createPhoto')]
    public function createPhotoForDemo(EntityManagerInterface $em, Request $request, PhotoFormHandler $photoFormHandler) {

        $photo = new Photo();
        $form = $this->createForm(PhotoType::class, $photo);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

           $photoFormHandler->uploadFilesFromForm($form);
            $em->persist($photo);
            $em->flush();
        }

        return $this->render('pages/admin/photo.html.twig', ['photoForm' => $form->createView()]);
    }



}
