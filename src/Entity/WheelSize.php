<?php

namespace App\Entity;

use App\Repository\WheelSizeRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: WheelSizeRepository::class)]
class WheelSize
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column]
    private ?float $numberOfInches = null;

    public function __toString(): string
    {
        return $this->numberOfInches . '"';
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNumberOfInches(): ?float
    {
        return $this->numberOfInches;
    }

    public function setNumberOfInches(float $numberOfInches): static
    {
        $this->numberOfInches = $numberOfInches;

        return $this;
    }
}
