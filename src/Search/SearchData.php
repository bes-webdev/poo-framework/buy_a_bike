<?php

namespace App\Search;

use App\Entity\Material;

class SearchData
{
    private $title;
    private ?Material $material = null;

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }



    public function getMaterial(): ?Material
    {
        return $this->material;
    }

    public function setMaterial(?Material $material): void
    {
        $this->material = $material;
    }



}
